<?xml version="1.0" encoding="UTF-8"?>
<package xmlns="http://pear.php.net/dtd/package-2.0" xmlns:tasks="http://pear.php.net/dtd/tasks-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" packagerversion="1.4.1" version="2.0" xsi:schemaLocation="http://pear.php.net/dtd/tasks-1.0 http://pear.php.net/dtd/tasks-1.0.xsd http://pear.php.net/dtd/package-2.0 http://pear.php.net/dtd/package-2.0.xsd">
  <name>ofFormConfirmPlugin</name>
  <channel>pear.symfony-project.com</channel>
  <summary>ofFormConfirmPlugin is a simple confirm and thank you page building classes.</summary>
  <description>ofFormConfirmPlugin is a simple confirm and thank you page building classes.</description>
  <lead>
    <name>openforce</name>
    <user>openforce</user>
    <email>OpenForce.Auth@gmail.com</email>
    <active>yes</active>
  </lead>
  <date>2008-08-26</date>
  <version>
    <release>0.3.1</release>
    <api>0.3.1</api>
  </version>
  <stability>
    <release>beta</release>
    <api>beta</api>
  </stability>
  <license uri="http://www.symfony-project.com/license">MIT license</license>
  <notes>-</notes>

  <contents>
  </contents>

  <dependencies>
    <required>
      <php>
        <min>5.1.0</min>
      </php>
      <pearinstaller>
        <min>1.4.1</min>
      </pearinstaller>
	   <package>
	    <name>symfony</name>
	    <channel>pear.symfony-project.com</channel>
	    <min>0.0.1</min>
	    <max>1.2.0</max>
	    <exclude>1.0.0</exclude>
	    <exclude>1.2.0</exclude>
	   </package>
	   <package>
	    <name>symfony</name>
	    <channel>pear.symfony-project.com</channel>
	    <min>1.1.0</min>
	    <max>1.2.0</max>
	    <exclude>1.2.0</exclude>
	   </package>
    </required>
  </dependencies>

  <phprelease>
  </phprelease>

  <changelog>
  </changelog>
</package>
