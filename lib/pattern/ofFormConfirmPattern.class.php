<?php

class ofFormConfirmPattern
{
	
	public static function standard(sfForm $form, sfAction $action, $key)
	{
	  	
		$context = sfContext::getInstance();
		if($defaults = $context->getUser()->getFlash($key))
		{
			$form->setDefaults($defaults);
		}
		
	  	if($context->getRequest()->isMethod('post'))
	  	{
	  		$form->bind($param = $action->getRequestParameter($key));
	  		
	  		if($form->isValid())
	  		{
	  			$context->getUser()->setFlash($key, $param);
	  			return sfView::SUCCESS;
	  			
	  		}
	  		
	  	}
	  	
	  	return sfView::INPUT;
	}
	
	
}