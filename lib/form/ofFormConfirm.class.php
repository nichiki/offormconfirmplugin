<?php

class ofFormConfirm
{
	/**
	 * sfForm object
	 *
	 * @var sfForm
	 */
	protected $form;
	protected $fields;
	protected $disabledFields;

	protected $formatRow = '<tr><th>%1$s</th><td>%2$s</d></tr>';

	protected $unescapedFields = array();

	protected $callbacks = array();
	
	/**
	 * event dispatcher
	 *
	 * @var sfEventDispatcher
	 */
	protected $dispatcher;
	
	public function __construct(sfForm $form, $disabledFields = array())
	{
		$this->form = $form;

		$this->fields = $this->form->getWidgetSchema()->getFields();
		$this->disabledFields = $disabledFields;
		
		$this->dispatcher = sfContext::getInstance()->getEventDispatcher();

	}
	
	public function setEventDispatcher(sfEventDispatcher $dispatcher)
	{
		$this->dispatcher = $dispatcher;
	}
	

	/**
	 * set Row Format
	 *
	 * %1$s => label
	 * %2$s => value
	 *
	 * @param string $format
	 */
	public function setFormatRow($format)
	{
		$this->formatRow = $format;
	}

	public function render()
	{
		$ret = "";

		foreach($this->fields as $fieldName => $fieldObject)
		{
			$ret .= $this->renderRow($fieldName);
		}

		return $ret;

	}

	public function setCallBack($name, $class, $method)
	{
		$this->callbacks[$name] = array($class, $method);
	}

	public function renderValue($fieldName)
	{
		$method = "export".get_class($this->form[$fieldName]->getWidget());

		if(isset($this->callbacks[$fieldName]))
		{
			$value = call_user_func($this->callbacks[$fieldName], $fieldName);
		}else{
			$value = $this->$method($fieldName);
			
			if(!isset($this->unescapedFields[$fieldName]))
			{
				$value = $this->form->getWidgetSchema()->escapeOnce($value);
			}
		}
		
		return $value;
	}

	public function renderRow($fieldName)
	{

		if(in_array($fieldName, $this->disabledFields))
		{
			return null;
		}
		$label = $this->form->getWidgetSchema()->getLabel($fieldName);
		$value = $this->renderValue($fieldName);

		return sprintf($this->formatRow, $label, $value);
	}

	public function exportsfWidgetFormSelect($name)
	{
		$widget = $this->form[$name]->getWidget();
		/* @var $widget sfWidgetFormSelect */
		$choices = $widget->getOption('choices');

		if($widget->getOption('multiple'))
		{
			foreach($this->form->getValue($name) as $key)
			{
				$ret[] = $choices[$key];
			}
				
			return join($ret, ", ");
		}

		return @$choices[$this->form->getValue($name)];
	}

	public function exportsfWidgetFormInputHidden($name)
	{
		return null;
	}

	public function exportsfWidgetFormInputCheckbox($name)
	{
		$this->unescapedFields[$name] = true;
		$widget = $this->form[$name]->getWidget();
		/* @var $widget sfWidgetFormInputCheckbox */
		return $widget->renderTag("img", array(
			'src'=> sfContext::getInstance()->getRequest()->getRelativeUrlRoot().("/sf/sf_admin/images/ok.png"),
			'alt'=> 'checked',
			'title' => 'checked'
			));


	}

	public function exportsfWidgetFormSelectMany($name)
	{
		/* @var $widget sfWidgetFormSelectMany */
		return $this->exportsfWidgetFormSelect($name);
	}

	public function exportsfWidgetFormSelectRadio($name)
	{
		/* @var $widget sfWidgetFormSelectRadio */
		return $this->exportsfWidgetFormSelect($name);
	}

	public function exportsfWidgetFormInputPassword($name)
	{
		/* @var $widget sfWidgetFormInputPassword */
		return "******";
	}

	public function escape($fieldName)
	{
		unset($this->unescapedFields[$fieldName]);
	}

	public function escapeAll()
	{
		$this->unescapedFields = array();
	}

	public function unescape($fieldName)
	{
		$this->unescapedFields[$fieldName] = true;
	}

	public function unescapeAll()
	{

		foreach($this->fields as $fieldName => $objectt)
		{
			$this->unescapedFields[$fieldName] = true;
		}

	}

	public function __call($method, $arguments)
	{
		$event = $this->dispatcher->notifyUntil(new sfEvent($this, 'form_confirm.export_widget_value', array(
      		'method'    => $method, 
      		'arguments' => array(
							$this->form, //sfForm Object
							$arguments[0]//fieldName
						)
		)));
		if (!$event->isProcessed())
		{
			
			return $this->form->getValue($arguments[0]);
		}
		
		return $event->getReturnValue();
	}


}